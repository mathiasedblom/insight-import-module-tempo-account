# README #

1) Clone the repository
2) Download Insight and Tempo Timesheet and unpack the insight.jar and tempo-account.jar and install them in your local repository. Needed to be able to compile the plugin.
3) Build the plugin
4) Install it in an JIRA environment where Insight 5.0 (or above) is running.

Read more about the import functionality here: https://documentation.riada.se/display/IDV5/General+Import+Concepts