if (typeof RLABS == 'undefined') {
	RLABS = {};
}
if (typeof RLABS.WorkflowFunction == 'undefined') {
	RLABS.WorkflowFunction = {};
}

RLABS.WorkflowFunction.setupSpecificChoice = function(li, ownCancelButton, type) {
	RLABS.WorkflowFunction.abstractSetupSpecificChoice(li, ownCancelButton, type);
};

AJS.$(document).ready(function() {	
	
	/* Set AUI class to get nicer styling */
	if (!AJS.$("form").hasClass("aui")) {
		AJS.$("form").addClass("aui");
	}

	var rlabsType = AJS.$("#rlabs-type");
	if (rlabsType.val() == "") {

		/* Get default and set select header */
		
		RLABS.WorkflowFunction.setupSelectHeader();
		
		/* Fix button container */
		RLABS.WorkflowFunction.setSelectButtons();
		
		AJS.$('.rlabs-scriptlink').bind('click', function(e) {
			
			var target = AJS.$(e.currentTarget);
			rlabsType.val(target.attr("data-type"));
			RLABS.WorkflowFunction.setupSpecificChoice(target.parent(), true, target.attr("data-type"));
			RLABS.WorkflowFunction.setAddParameterButtons();

		});
	} else {
		var li = AJS.$("#rlabs-scriptlink-" + rlabsType.val()).parent();
		RLABS.WorkflowFunction.setupSpecificChoice(li, false, rlabsType.val());		
	}
	
});


