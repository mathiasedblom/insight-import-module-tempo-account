if (typeof RLABS == 'undefined') {
	RLABS = {};
}
if (typeof RLABS.WorkflowFunction == 'undefined') {
	RLABS.WorkflowFunction = {};
}

RLABS.WorkflowFunction.jiraScriptHeader = null;
RLABS.WorkflowFunction.jiraScriptDesc = null;

RLABS.WorkflowFunction.abstractSetupSpecificChoice = function(li, ownCancelButton, type) {
	/* Reset header */
	RLABS.WorkflowFunction.setupDefaultHeader();
	
	AJS.$('#rlabs-script-list li').each(function () {
		AJS.$(this).hide();
	})

	li.show();
	li.find(".rlabs-script-args").show();
	
	if (ownCancelButton) {
		var cancelButton = AJS.$("#cancelButton");
		cancelButton.hide();
		
		var rlabsCancelButtonLength = AJS.$("#rlabsCancelButton").length;
		if (!rlabsCancelButtonLength) {
			cancelButton.parent().append(RLABS.tpl.workflowfunction.general.renderCancelButton());
			AJS.$("#rlabsCancelButton").bind('click', function(e) {
				li.hide();
				li.find(".rlabs-script-args").hide();
				AJS.$('#rlabs-script-list li').each(function () {
					AJS.$(this).show();
				})
				cancelButton.show();
				AJS.$(this).remove();
				
				/* Set select header */
				RLABS.WorkflowFunction.setupSelectHeader();
				
				/* Fix button container */
				RLABS.WorkflowFunction.setSelectButtons();
			});
		}
	}
	
}

RLABS.WorkflowFunction.setupSelectHeader = function() {
	RLABS.WorkflowFunction.jiraScriptHeader = AJS.$("form").find("table tr.titlerow td.jiraformheader h3").html();
	RLABS.WorkflowFunction.jiraScriptDesc = AJS.$("form").find("table tr.descriptionrow td.jiraformheader div").html();

	AJS.$("form").find("table tr.titlerow td.jiraformheader h3").html(AJS.I18n.getText('rlabs.insight.i18n.post_function.select_script.title'));
	AJS.$("form").find("table tr.descriptionrow td.jiraformheader div").html(AJS.I18n.getText('rlabs.insight.i18n.post_function.select_script.description'));
}

RLABS.WorkflowFunction.setupDefaultHeader = function() {
	AJS.$("form").find("table tr.titlerow td.jiraformheader h3").html(RLABS.WorkflowFunction.jiraScriptHeader);
	AJS.$("form").find("table tr.descriptionrow td.jiraformheader div").html(RLABS.WorkflowFunction.jiraScriptDesc);
}

RLABS.WorkflowFunction.setSelectButtons = function() {
	AJS.$("#add_submit").hide();
	AJS.$("#update_submit").hide();
	AJS.$("#cancelButton").removeClass("aui-button-link");
	AJS.$("#rlabsCancelButton").removeClass("aui-button-link");
}

RLABS.WorkflowFunction.setAddParameterButtons = function() {
	
	AJS.$("#add_submit").show();
	AJS.$("#update_submit").show();
	AJS.$("#cancelButton").addClass("aui-button-link");
	AJS.$("#rlabsCancelButton").addClass("aui-button-link");
	
}


