if (typeof RLABS == 'undefined') {
    RLABS = {};
}
if (typeof RLABS.Panel == 'undefined') {
    RLABS.Panel = {};
}
if (typeof RLABS.Panel.TempoAccount == 'undefined') {
    RLABS.Panel.TempoAccount = {};
}

RLABS.Panel.TempoAccount.init = function (accountId) {

    AJS.$.ajax({
        type: "GET",
        url: RLABS.Ajax.CONTEXT_PATH + '/rest/insight-tempo-accounts/1.0/account/' + accountId + '/objects',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (objects) {
        	
        	var objectKeys = "";
        	AJS.$.each(objects, function (i, object) {
        		if (objectKeys !== "") {
        			objectKeys += ",";
        		}
        		objectKeys += object.objectKey;
            });
        	
        	if (objectKeys == "") {
        		AJS.$("#rlabs-insight-panel").html(RLABS.tpl.panel.tempoaccount.renderOverview({data: []}));
        	} else {
        	
	            AJS.$.ajax({
	                type: "GET",
	                url: RLABS.Ajax.buildRestUrl('/object?objectKeys=' + objectKeys + "&includeAttributes=true"),
	                contentType: "application/json; charset=utf-8",
	                dataType: "json",
	                success: function (data) {
	                	
	                	AJS.$("#rlabs-insight-panel").html(RLABS.tpl.panel.tempoaccount.renderOverview({data: data}));
	        	
			            var showAllAttributeValuesInnerDialog = function ($showAllEl, options) {
			        		
			                var detailsViewData = options[2];
			                var _objId = AJS.$($showAllEl).data("obj-id");
			                var _objAttrId = AJS.$($showAllEl).data("obj-attr-id");
			
			                var _object, _objectAttribute, _objectTypeAttribute;
			
			                _object = detailsViewData;
			                _object.forEach(function (objIterate) {
			                    _objectAttribute = _.findWhere(objIterate.attributes, {id: _objAttrId});
			                    _objectTypeAttribute = _objectAttribute.objectTypeAttribute;
			
			                    if (_objectTypeAttribute.type != 1) {
			                        if (!RLABS.InlineDialog.Dialogs[_objAttrId]) {
			                            RLABS.Objecttype.Attribute.CreateShowAllPopUpWhenNotObject($showAllEl, _objAttrId, objIterate, _objectTypeAttribute, _objectAttribute);
			                        }
			                        RLABS.InlineDialog.Dialogs[_objAttrId].show();
			
			                    } else {
			                        var objectAttributeValues = [];
			                        _objectAttribute.objectAttributeValues.forEach(function (value) {
			                            objectAttributeValues.push(value.referencedObject)
			                        });
			
			                        var showAllObjectsView = new RLABS.Objecttype.Attribute.ShowAllObjectsView();
			                        showAllObjectsView.options.objectAttributeValues = objectAttributeValues;
			                        showAllObjectsView.options.name = _objectTypeAttribute.name;
			                        showAllObjectsView.render();
			                    }
			                })
			            };
			            
			            AJS.$("#rlabs-insight-panel").find(".js-rlabs-show-more-attribute-values").click(function (e) {
			                RLABS.Utils.preventDefault(e);
			                showAllAttributeValuesInnerDialog(e.currentTarget, [e.currentTarget, {'viewMode': 'detail'}, data]);
			                return false;
			            });
			
			            AJS.$.each(AJS.$("#rlabs-insight-panel").find(".rlabs-value-container[data-key]"), function(i, refObjectValueContainer) {
			                var obj = AJS.$(refObjectValueContainer);
			                RLABS.ObjectDialog.initDialog(obj, obj.attr("data-key"), obj.attr("id"));
			            });
			
	
	                }
	            }, 'Insight Objects');
        	}
        }
    }, 'Tempo Objects');
};
