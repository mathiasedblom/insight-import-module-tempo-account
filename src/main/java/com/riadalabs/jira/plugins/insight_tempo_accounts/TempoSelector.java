package com.riadalabs.jira.plugins.insight_tempo_accounts;

import java.util.HashMap;
import java.util.Map;

public enum TempoSelector {
    ACCOUNT("ACCOUNT"),
    CUSTOMER("CUSTOMER"),
    CATEGORY("CATEGORY"),
    CATEGORY_LEGACY_TYPE("COLLECTOR_TYPE"), // Typo since 1.0
    CATEGORY_TYPE("CATEGORY_TYPE"),
    UNKNOWN("");

    private String selector;
    private static Map<String, TempoSelector> operatorToEnumMapping;

    private TempoSelector(String selector) {
        this.selector = selector;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    public static TempoSelector getInstance(String operator) {
        if (operatorToEnumMapping == null) {
            initMapping();
        }

        TempoSelector snowSelector = operatorToEnumMapping.get(operator.toUpperCase());
        return snowSelector != null ? snowSelector : TempoSelector.UNKNOWN;
    }

    private static void initMapping() {
        operatorToEnumMapping = new HashMap<>();
        for (TempoSelector s : values()) {
            operatorToEnumMapping.put(s.selector, s);
        }
    }

}
