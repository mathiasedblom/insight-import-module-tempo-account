package com.riadalabs.jira.plugins.insight_tempo_accounts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.AbstractInsightImportModule;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ImportComponentException;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ImportDataHolder;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.InMemoryDataHolder;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.InsightImportModule;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ModuleOTSelector;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.TemplateImportConfiguration;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.TemplateImportConfiguration.AttributeMapping;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.TemplateImportConfiguration.ObjectTypeMapping;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.DataEntry;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.ObjectTypeAttributeModuleExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.ObjectTypeModuleExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.ReferencedObjectTypeAttributeModuleExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.SimpleDataEntry;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.InsightSchemaExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.ObjectTypeAttributeExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.ObjectTypeExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.validation.ValidationResult;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.TempoContentManager;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.TempoStructureManager;
import com.riadalabs.jira.plugins.insight_tempo_accounts.util.I18n;
import com.riadalabs.jira.plugins.insight_tempo_accounts.util.I18nFactoryService;

/**
 * 
 * @author Mathias Edblom
 * @since 5.0
 *
 */
public class TempoAccountImportModule extends AbstractInsightImportModule<TempoConfiguration>
        implements InsightImportModule<TempoConfiguration> {

    public static final String TEMPO_IMPORT_MODULE_KEY = "rlabs-import-type-tempo-accounts";

    private final JiraAuthenticationContext authCtx;
    private final I18nFactoryService i18nFactoryService;
    private static final Logger logger = LoggerFactory.getLogger(TempoAccountImportModule.class);

    public TempoAccountImportModule(final JiraAuthenticationContext authCtx,
            final I18nFactoryService i18nFactoryService) {
        this.authCtx = authCtx;
        this.i18nFactoryService = i18nFactoryService;
    }

    @Override
    public ImportDataHolder dataHolder(TempoConfiguration configuration, ModuleOTSelector moduleOTSelector)
            throws ImportComponentException {

        try {
            List<DataEntry> dataEntries = new ArrayList<>();
            if (!moduleOTSelector.isEmpty()) {
                TempoContentManager tempoContentManager = new TempoContentManager();
                List<Map<DataLocator, List<String>>> maps = tempoContentManager.getDataEntries(moduleOTSelector);

                for (Map<DataLocator, List<String>> map : maps) {
                    SimpleDataEntry simpleDataEntry = new SimpleDataEntry(map);
                    dataEntries.add(simpleDataEntry);
                }
            }

            return InMemoryDataHolder.createInMemoryDataHolder(dataEntries);
        } catch (Exception e) {
            logger.error("Unable to fetch data holder from TEMPO using conf " + configuration, e);
            throw new ImportComponentException("Unable to fetch data holder from Tempo Account Module", e);
        }

    }

    @Override
    public TempoConfiguration importModuleConfigurationTemplate() {
        return new TempoConfiguration();
    }

    @Override
    public InsightSchemaExternal predefinedStructure(TempoConfiguration configuration) {
        return (new TempoStructureManager()).getPredefinedStructure();
    }

    @Override
    public TemplateImportConfiguration templateImportConfiguration(TempoConfiguration configuration) {
        try {
            InsightSchemaExternal insightSchemaExternal = (new TempoStructureManager()).getPredefinedStructure();
            List<TemplateImportConfiguration.ObjectTypeMapping> otMappings = getObjectTypeMappings(
                    insightSchemaExternal.getObjectSchema().getObjectTypes().get(0));
            return TemplateImportConfiguration.createConfigWithMapping(otMappings);
        } catch (Exception e) {
            logger.error("Unable to prepare insight external data for template configuration " + configuration, e);
            throw new ImportComponentException(e);
        }
    }

    private List<ObjectTypeMapping> getObjectTypeMappings(ObjectTypeExternal externalFormat) {

        List<ObjectTypeMapping> otMappings = new ArrayList<>();
        ObjectTypeMapping otMapping = mappingForExternalFormat(externalFormat);
        if (otMapping != null) {
            otMappings.add(otMapping);
        }
        if (externalFormat.getObjectTypeChildren() != null) {
            for (ObjectTypeExternal objectTypeExternal : externalFormat.getObjectTypeChildren()) {
                otMappings.addAll(getObjectTypeMappings(objectTypeExternal));
            }
        }

        return otMappings;
    }

    private ObjectTypeMapping mappingForExternalFormat(ObjectTypeExternal externalFormat) {

        ObjectTypeModuleExternal objectTypeImportExternal = (ObjectTypeModuleExternal) externalFormat;

        if (objectTypeImportExternal.isCreateOTConfiguration()) {
            List<ObjectTypeAttributeExternal> objectTypeAttributeExternals = objectTypeImportExternal
                    .getObjectTypeAttributes();
            ObjectTypeMapping otMapping = new ObjectTypeMapping();

            otMapping.setObjectTypeName(objectTypeImportExternal.getName());
            otMapping.setSelector(objectTypeImportExternal.getSelector());
            otMapping.setHandleMissingObjects(objectTypeImportExternal.getHandleMissingObjects());

            otMapping.setHavingAttributeNames(objectTypeAttributeExternals.stream()
                    .map(ObjectTypeAttributeExternal::getName).collect(Collectors.toList()));
            List<AttributeMapping> attributeMappings = new ArrayList<>();

            for (ObjectTypeAttributeExternal objectTypeAttributeExternal : objectTypeAttributeExternals) {
                AttributeMapping attributeMapping = new AttributeMapping();

                ObjectTypeAttributeModuleExternal objectTypeAttributeImportExternal = (ObjectTypeAttributeModuleExternal) objectTypeAttributeExternal;

                attributeMapping.setAttributeName(objectTypeAttributeExternal.getName());
                attributeMapping.setAttributeLocators(objectTypeAttributeImportExternal.getDataLocators());

                if (objectTypeAttributeImportExternal instanceof ReferencedObjectTypeAttributeModuleExternal) {
                    attributeMapping.setObjectMappingIQL(
                            ((ReferencedObjectTypeAttributeModuleExternal) objectTypeAttributeImportExternal)
                                    .getObjectMappingIQL());
                }

                attributeMapping.setExternalIdPart(objectTypeAttributeImportExternal.isIdentifier());
                attributeMappings.add(attributeMapping);
            }

            otMapping.setAttributesMapping(attributeMappings);

            return otMapping;
        }

        return null;
    }

    /**
     * Retrieves all the unique attributes from the LDAP structure in order to map them with Insight Attributes
     * 
     * @param
     * @return
     * @throws ImportComponentException
     */
    @Override
    public List<DataLocator> fetchDataLocators(TempoConfiguration configuration, ModuleOTSelector moduleOTSelector)
            throws ImportComponentException {

        if (moduleOTSelector.isEmpty()) {
            new ArrayList<>();
        }

        TempoStructureManager tempoStructureManager = new TempoStructureManager();
        return tempoStructureManager.getDataLocators(moduleOTSelector);

    }

    @Override
    public TempoConfiguration convertConfigurationFromJSON(String jsonString) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(jsonString, TempoConfiguration.class);
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to parse configuration as TempoConfiguration " + jsonString, e);
        }

    }

    @Override
    public ValidationResult validateOTConfiguration(TempoConfiguration configuration,
            ModuleOTSelector moduleOTSelector) {

        Map<String, String> error = new HashMap<>();
        if (TempoSelector.getInstance(moduleOTSelector.getSelector()).equals(TempoSelector.UNKNOWN)) {
            error.put("selector",
                    getI18n().getText("rlabs.insight-tempo-accounts.i18n.import.validation.selectortype",
                            Stream.of(TempoSelector.values())
                                    .filter(sel -> (!sel.equals(TempoSelector.UNKNOWN)
                                            && !sel.equals(TempoSelector.CATEGORY_LEGACY_TYPE)))
                                    .map(TempoSelector::getSelector).collect(Collectors.toList()).toString()));
        }
        if (error.isEmpty()) {
            return ValidationResult.OK();
        } else {
            return ValidationResult.error(error);
        }

    }

    @Override
    public ValidationResult validateDependencies() {
        Plugin tempoTimeSheet = ComponentAccessor.getPluginAccessor().getEnabledPlugin("is.origo.jira.tempo-plugin");
        if (tempoTimeSheet != null) {
            return ValidationResult.OK();
        }
        return ValidationResult.error("dependency", getI18n().getText("rlabs.insight-tempo-accounts.i18n.warning"));

    }

    private I18n getI18n() {
        return i18nFactoryService.getI18n();
    }

}
