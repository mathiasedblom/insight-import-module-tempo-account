package com.riadalabs.jira.plugins.insight_tempo_accounts.manager;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.riadalabs.jira.plugins.insight_tempo_accounts.TempoSelector;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl.AccountService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl.CategoryService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl.CategoryTypeService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl.CustomerService;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ModuleOTSelector;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.ObjectTypeAttributeModuleExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.ObjectTypeModuleExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.ReferencedObjectTypeAttributeModuleExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.IconExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.InsightSchemaExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.ObjectSchemaExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.ObjectTypeExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.ReferenceTypeExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.StatusTypeExternal;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeAttributeBean;
import com.riadalabs.jira.plugins.insight.services.model.StatusTypeBean;

public class TempoStructureManager extends TempoContentManager {

    private final Logger logger = LoggerFactory.getLogger(TempoStructureManager.class);
    private static final int OBJECT_SCHEMA_ID = 1; /* Just a fake id */
    private int referenceTypeSequenceNumber;
    private int objectTypeSequenceNumber;

    public enum Status {
        OPEN("Open", StatusTypeBean.STATUS_CATEGORY_ACTIVE),
        CLOSED("Closed", StatusTypeBean.STATUS_CATEGORY_PENDING),
        ARCHIVED("Archived", StatusTypeBean.STATUS_CATEGORY_INACTIVE);

        String name;
        int category;
        private static Map<String, Status> operatorToEnumMapping;

        private Status(String name, int category) {
            this.name = name;
            this.category = category;
        }

        public static Status getInstance(String source) {
            if (operatorToEnumMapping == null) {
                initMapping();
            }
            return operatorToEnumMapping.get(source);
        }

        private static void initMapping() {
            operatorToEnumMapping = new HashMap<>();
            for (Status s : values()) {
                operatorToEnumMapping.put(s.name, s);
            }
        }

        public String getName() {
            return name;
        }

        public int getCategory() {
            return category;
        }

    }

    public TempoStructureManager() {
        this.referenceTypeSequenceNumber = 1;
        this.objectTypeSequenceNumber = 1;
    }

    public InsightSchemaExternal getPredefinedStructure() {

        InsightSchemaExternal insightSchemaExternal = new InsightSchemaExternal();

        ObjectSchemaExternal objectSchemaExternal = new ObjectSchemaExternal();
        insightSchemaExternal.setObjectSchema(objectSchemaExternal);
        objectSchemaExternal.setId(OBJECT_SCHEMA_ID);

        List<StatusTypeExternal> statusTypes = new ArrayList<>();
        for (Status status : Status.values()) {
            StatusTypeExternal statusTypeExternal = new StatusTypeExternal();
            statusTypeExternal.setName(status.getName());
            statusTypeExternal.setCategory(status.getCategory());
            statusTypeExternal.setObjectSchemaId(OBJECT_SCHEMA_ID);
            statusTypes.add(statusTypeExternal);
        }

        insightSchemaExternal.setStatusTypes(statusTypes);

        IconExternal icon = getIcon("Tempo");
        insightSchemaExternal.getIcons().add(icon);

        ObjectTypeExternal rootObjectTypeExternal = new ObjectTypeModuleExternal(null, false, null);
        objectSchemaExternal.getObjectTypes().add(rootObjectTypeExternal);
        rootObjectTypeExternal.setId(objectTypeSequenceNumber++);
        rootObjectTypeExternal.setName("Tempo");
        rootObjectTypeExternal.setIcon(icon);

        ObjectTypeExternal tempoAcccountsObjectTypeExternal = new ObjectTypeModuleExternal(
                TempoSelector.ACCOUNT.getSelector(), true, null);
        rootObjectTypeExternal.getObjectTypeChildren().add(tempoAcccountsObjectTypeExternal);
        tempoAcccountsObjectTypeExternal.setId(objectTypeSequenceNumber++);
        tempoAcccountsObjectTypeExternal.setName("Tempo Accounts");
        tempoAcccountsObjectTypeExternal.setIcon(icon);

        ObjectTypeExternal tempoCustomersObjectTypeExternal = new ObjectTypeModuleExternal(
                TempoSelector.CUSTOMER.getSelector(), true, null);
        rootObjectTypeExternal.getObjectTypeChildren().add(tempoCustomersObjectTypeExternal);
        tempoCustomersObjectTypeExternal.setId(objectTypeSequenceNumber++);
        tempoCustomersObjectTypeExternal.setName("Tempo Customers");
        tempoCustomersObjectTypeExternal.setIcon(icon);

        ObjectTypeExternal tempoCategoriesObjectTypeExternal = new ObjectTypeModuleExternal(
                TempoSelector.CATEGORY.getSelector(), true, null);
        rootObjectTypeExternal.getObjectTypeChildren().add(tempoCategoriesObjectTypeExternal);
        tempoCategoriesObjectTypeExternal.setId(objectTypeSequenceNumber++);
        tempoCategoriesObjectTypeExternal.setName("Tempo Categories");
        tempoCategoriesObjectTypeExternal.setIcon(icon);

        ObjectTypeExternal tempoCategoryTypeObjectTypeExternal = new ObjectTypeModuleExternal(
                TempoSelector.CATEGORY_TYPE.getSelector(), true, null);
        tempoCategoriesObjectTypeExternal.getObjectTypeChildren().add(tempoCategoryTypeObjectTypeExternal);
        tempoCategoryTypeObjectTypeExternal.setId(objectTypeSequenceNumber++);
        tempoCategoryTypeObjectTypeExternal.setName("Category Type");
        tempoCategoryTypeObjectTypeExternal.setIcon(icon);

        /* Add account attributes */
        addAccountsAttributes(insightSchemaExternal, tempoAcccountsObjectTypeExternal, tempoCustomersObjectTypeExternal,
                tempoCategoriesObjectTypeExternal);

        /* Add customers attributes */
        addCustomersAttributes(tempoCustomersObjectTypeExternal);

        /* Add categories attributes */
        addCategoriesAttributes(insightSchemaExternal, tempoCategoriesObjectTypeExternal,
                tempoCategoryTypeObjectTypeExternal);

        /* Add category type attributes */
        addCategoryTypeAttributes(tempoCategoryTypeObjectTypeExternal);

        return insightSchemaExternal;
    }

    private IconExternal getIcon(String objectTypeName) {
        IconExternal iconExternal = new IconExternal();
        iconExternal.setName(objectTypeName);
        iconExternal.setObjectSchemaId(OBJECT_SCHEMA_ID);

        InputStream in = TempoStructureManager.class.getClassLoader()
                .getResourceAsStream("/images/icon/" + objectTypeName + ".png");

        try {
            iconExternal.setImage48(IOUtils.toByteArray(in));
            iconExternal.setImage16(iconExternal.getImage48());
        } catch (Exception ioe) {
            try {
                in.close();
            } catch (IOException e) {
            }
        }

        return iconExternal;

    }

    private void addCategoriesAttributes(InsightSchemaExternal insightSchemaExternal, ObjectTypeExternal objectType,
            ObjectTypeExternal tempoCategoryTypeObjectType) {

        // Name
        addTextObjectTypeAttribute("Name", true, objectType, false, false, CategoryService.name);

        // Tempo ID
        addValueObjectTypeAttribute("Tempo Id", ObjectTypeAttributeBean.DefaultType.INTEGER, null, false, objectType,
                true, CategoryService.id);

        // Tempo Key
        addTextObjectTypeAttribute("Tempo Key", false, objectType, false, false, CategoryService.key);

        // Category Type
        addReferenceObjectTypeAttribute(insightSchemaExternal, "Category Type", objectType, tempoCategoryTypeObjectType,
                "Category Type", false, false, "\"Tempo Id\" = ${" + CategoryService.categoryType.getLocator() + "}",
                false, CategoryService.categoryType);

    }

    private void addCustomersAttributes(ObjectTypeExternal objectType) {

        // Name
        addTextObjectTypeAttribute("Name", true, objectType, false, false, CustomerService.name);

        // Tempo ID
        addValueObjectTypeAttribute("Tempo Id", ObjectTypeAttributeBean.DefaultType.INTEGER, null, false, objectType,
                true, CustomerService.id);

        // Tempo Key
        addTextObjectTypeAttribute("Tempo Key", false, objectType, false, false, CustomerService.key);

    }

    private void addCategoryTypeAttributes(ObjectTypeExternal objectType) {

        // Name
        addTextObjectTypeAttribute("Name", true, objectType, false, false, CategoryTypeService.name);

        // Tempo ID
        addValueObjectTypeAttribute("Tempo Id", ObjectTypeAttributeBean.DefaultType.INTEGER, null, false, objectType,
                true, CategoryTypeService.id);

        // Color
        addTextObjectTypeAttribute("Color", false, objectType, false, false, CategoryTypeService.color);

    }

    private void addAccountsAttributes(InsightSchemaExternal insightSchemaExternal, ObjectTypeExternal objectType,
            ObjectTypeExternal tempoCustomersObjectType, ObjectTypeExternal tempoCategoryObjectType) {

        // Name
        addTextObjectTypeAttribute("Name", true, objectType, false, false, AccountService.name);

        // Tempo ID
        addValueObjectTypeAttribute("Tempo Id", ObjectTypeAttributeBean.DefaultType.INTEGER, null, false, objectType,
                true, AccountService.id);

        // Tempo Key
        addTextObjectTypeAttribute("Tempo Key", false, objectType, false, false, AccountService.key);

        // Global
        addBooleanObjectTypeAttribute("Global", objectType, false, AccountService.global);

        // Monthly budget
        addValueObjectTypeAttribute("Monthly Budget", ObjectTypeAttributeBean.DefaultType.INTEGER, "$", true,
                objectType, false, AccountService.monthlyBudget);

        // Lead
        addUserObjectTypeAttribute("Lead", objectType, false, AccountService.lead);

        // Contact
        addUserObjectTypeAttribute("Contact", objectType, false, AccountService.contact);

        // Customer
        addReferenceObjectTypeAttribute(insightSchemaExternal, "Customer", objectType, tempoCustomersObjectType,
                "Customer", false, false, "\"Tempo Id\" = ${" + AccountService.customer.getLocator() + "}", false,
                AccountService.customer);

        // Category
        addReferenceObjectTypeAttribute(insightSchemaExternal, "Category", objectType, tempoCategoryObjectType,
                "Category", false, false, "\"Tempo Id\" = ${" + AccountService.category.getLocator() + "}", false,
                AccountService.category);

        // Status
        addStatusObjectTypeAttribute("Status", objectType, false, AccountService.status);

        // Linked Projects
        addProjectObjectTypeAttribute("Linked Projects", objectType, false, AccountService.projectIds);

    }

    private void addStatusObjectTypeAttribute(String name, ObjectTypeExternal objectTypeExternal, boolean identifier,
            DataLocator dataLocator) {
        ObjectTypeAttributeModuleExternal ota = new ObjectTypeAttributeModuleExternal(dataLocator, identifier);
        ota.setType(ObjectTypeAttributeBean.Type.STATUS.getTypeId());
        ota.setName(name);
        objectTypeExternal.getObjectTypeAttributes().add(ota);
    }

    private void addValueObjectTypeAttribute(String name, ObjectTypeAttributeBean.DefaultType defaultType,
            String suffix, boolean summable, ObjectTypeExternal objectTypeExternal, boolean identifier,
            DataLocator dataLocator) {
        ObjectTypeAttributeModuleExternal ota = new ObjectTypeAttributeModuleExternal(dataLocator, identifier);
        ota.setType(ObjectTypeAttributeBean.Type.DEFAULT.getTypeId());
        ota.setDefaultTypeId(defaultType.getDefaultTypeId());
        ota.setName(name);
        if (suffix != null) {
            ota.setSuffix(suffix);
        }
        ota.setSummable(summable);
        objectTypeExternal.getObjectTypeAttributes().add(ota);
    }

    private void addReferenceObjectTypeAttribute(InsightSchemaExternal insightSchemaExternal, String name,
            ObjectTypeExternal objectTypeExternal, ObjectTypeExternal referenceObjectTypeExternal,
            String referenceTypeName, boolean multiple, boolean includeChilds, String mappingIQL, boolean identifier,
            DataLocator dataLocator) {

        ObjectTypeAttributeModuleExternal ota = new ReferencedObjectTypeAttributeModuleExternal(dataLocator, identifier,
                mappingIQL);
        ota.setType(ObjectTypeAttributeBean.Type.REFERENCED_OBJECT.getTypeId());
        ota.setName(name);
        ota.setReferenceObjectTypeId(referenceObjectTypeExternal.getId());

        ReferenceTypeExternal referenceTypeExternal = getReferenceTypeExternal(referenceTypeName);
        referenceTypeExternal.setId(this.referenceTypeSequenceNumber++);
        insightSchemaExternal.getReferenceTypes().add(referenceTypeExternal);

        ota.setReferenceType(referenceTypeExternal);
        ota.setIncludeChildObjectTypes(includeChilds);
        if (multiple) {
            ota.setMaximumCardinality(ObjectTypeAttributeBean.CARDINALITY_MAXIMUM_UNLIMITED);
        }

        objectTypeExternal.getObjectTypeAttributes().add(ota);
    }

    private ReferenceTypeExternal getReferenceTypeExternal(String name) {
        ReferenceTypeExternal referenceTypeExternal = new ReferenceTypeExternal();
        referenceTypeExternal.setName(name);
        referenceTypeExternal.setObjectSchemaId(OBJECT_SCHEMA_ID);
        return referenceTypeExternal;
    }

    private void addUserObjectTypeAttribute(String name, ObjectTypeExternal objectTypeExternal, boolean identifier,
            DataLocator dataLocator) {

        ObjectTypeAttributeModuleExternal ota = new ObjectTypeAttributeModuleExternal(dataLocator, identifier);
        ota.setType(ObjectTypeAttributeBean.Type.USER.getTypeId());
        ota.setName(name);
        objectTypeExternal.getObjectTypeAttributes().add(ota);
    }

    private void addProjectObjectTypeAttribute(String name, ObjectTypeExternal objectTypeExternal, boolean identifier,
            DataLocator dataLocator) {

        ObjectTypeAttributeModuleExternal ota = new ObjectTypeAttributeModuleExternal(dataLocator, identifier);
        ota.setType(ObjectTypeAttributeBean.Type.PROJECT.getTypeId());
        ota.setName(name);
        objectTypeExternal.getObjectTypeAttributes().add(ota);
    }

    private void addTextObjectTypeAttribute(String name, boolean label, ObjectTypeExternal objectTypeExternal,
            boolean unique, boolean identifier, DataLocator dataLocator) {

        ObjectTypeAttributeModuleExternal ota = new ObjectTypeAttributeModuleExternal(dataLocator, identifier);
        ota.setType(ObjectTypeAttributeBean.Type.DEFAULT.getTypeId());
        ota.setDefaultTypeId(ObjectTypeAttributeBean.DefaultType.TEXT.getDefaultTypeId());
        ota.setName(name);
        ota.setLabel(label);
        ota.setUniqueAttribute(unique);

        objectTypeExternal.getObjectTypeAttributes().add(ota);

    }

    private void addBooleanObjectTypeAttribute(String name, ObjectTypeExternal objectTypeExternal, boolean identifier,
            DataLocator dataLocator) {

        ObjectTypeAttributeModuleExternal ota = new ObjectTypeAttributeModuleExternal(dataLocator, identifier);
        ota.setType(ObjectTypeAttributeBean.Type.DEFAULT.getTypeId());
        ota.setDefaultTypeId(ObjectTypeAttributeBean.DefaultType.BOOLEAN.getDefaultTypeId());
        ota.setName(name);

        objectTypeExternal.getObjectTypeAttributes().add(ota);

    }

    public List<DataLocator> getDataLocators(ModuleOTSelector moduleOTSelector) {

        TempoManager tempoManager = getDataTypeManager(moduleOTSelector);
        List<DataLocator> dataLocators = null;
        if (tempoManager != null) {
            dataLocators = tempoManager.getDataLocators();
        }

        if (dataLocators != null) {
            Collections.sort(dataLocators);
            return dataLocators;
        }

        return new ArrayList<>();
    }
}
