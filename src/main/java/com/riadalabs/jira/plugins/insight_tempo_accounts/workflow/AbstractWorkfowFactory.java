package com.riadalabs.jira.plugins.insight_tempo_accounts.workflow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;

public abstract class AbstractWorkfowFactory extends AbstractWorkflowPluginFactory {

    protected final CustomFieldManager customFieldManager;

    public static final String CUSTOM_FIELD_TEMPO_ACCOUNT_TYPE = "com.tempoplugin.tempo-accounts:accounts.customfield";
    public static final String CUSTOM_FIELD_INSIGHT_PREFIX = "com.riadalabs.jira.plugins.insight:";

    public AbstractWorkfowFactory(final CustomFieldManager customFieldManager) {
        this.customFieldManager = customFieldManager;
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put("allInsightCustomFields", getAllInsightCustomFieldBeans());
        velocityParams.put("allTempoAccounts", getAllTempoAccounts());
        velocityParams.put("type", "");
    }

    /**
     * Find Insight custom fields that only select its configured object type. No object children.
     * 
     * @return list of insight custom fields
     */
    public List<CustomField> getAllTempoAccounts() {

        List<CustomField> customFields = customFieldManager.getCustomFieldObjects();
        List<CustomField> returnCustomFields = new ArrayList<CustomField>();

        for (CustomField customField : customFields) {
            if (customField.getCustomFieldType().getKey().equals(CUSTOM_FIELD_TEMPO_ACCOUNT_TYPE)) {
                returnCustomFields.add(customField);
            }
        }

        return returnCustomFields;
    }

    /**
     * Find Insight custom fields that only select its configured object type. No object children.
     * 
     * @return list of insight custom fields
     */
    public List<CustomField> getAllInsightCustomFieldBeans() {

        List<CustomField> customFields = customFieldManager.getCustomFieldObjects();
        List<CustomField> returnCustomFields = new ArrayList<CustomField>();
        for (CustomField customField : customFields) {
            if (customField.getCustomFieldType().getKey().startsWith(CUSTOM_FIELD_INSIGHT_PREFIX)) {
                returnCustomFields.add(customField);
            }
        }

        return returnCustomFields;
    }

}