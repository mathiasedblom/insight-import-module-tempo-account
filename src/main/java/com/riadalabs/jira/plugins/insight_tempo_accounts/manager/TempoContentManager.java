package com.riadalabs.jira.plugins.insight_tempo_accounts.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ModuleOTSelector;
import com.riadalabs.jira.plugins.insight_tempo_accounts.TempoSelector;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl.AccountService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl.CategoryService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl.CategoryTypeService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl.CustomerService;

public class TempoContentManager {

    private final Logger logger = LoggerFactory.getLogger(TempoContentManager.class);

    public TempoContentManager() {
    }

    public List<Map<DataLocator, List<String>>> getDataEntries(ModuleOTSelector moduleOTSelector) throws Exception {
        TempoManager tempoManager = getDataTypeManager(moduleOTSelector);
        return tempoManager != null ? tempoManager.getDataHolder() : new ArrayList<>();
    }

    public boolean validate(ModuleOTSelector moduleOTSelector) {
        TempoManager insightDiscoveryService = getDataTypeManager(moduleOTSelector);
        return insightDiscoveryService != null;
    }

    protected TempoManager getDataTypeManager(ModuleOTSelector moduleOTSelector) {

        TempoManager tempoManager = null;

        switch (TempoSelector.getInstance(moduleOTSelector.getSelector())) {
        case ACCOUNT:
            tempoManager = new AccountService();
            break;
        case CATEGORY:
            tempoManager = new CategoryService();
            break;
        case CATEGORY_TYPE:
        case CATEGORY_LEGACY_TYPE:
            tempoManager = new CategoryTypeService();
            break;
        case CUSTOMER:
            tempoManager = new CustomerService();
            break;
        default:
            break;
        }

        return tempoManager;
    }

}
