package com.riadalabs.jira.plugins.insight_tempo_accounts.rest;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ImportSourceConfigurationFacade;
import com.riadalabs.jira.plugins.insight.common.exception.InsightException;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight.services.imports.model.ImportSource;
import com.riadalabs.jira.plugins.insight.services.imports.model.ImportSourceOT;
import com.riadalabs.jira.plugins.insight.services.imports.model.ImportSourceOTAttr;
import com.riadalabs.jira.plugins.insight.services.model.ObjectBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeAttributeBean;
import com.riadalabs.jira.plugins.insight_tempo_accounts.TempoAccountImportModule;
import com.riadalabs.jira.plugins.insight_tempo_accounts.TempoSelector;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl.AccountService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.rest.model.ObjectEntry;

/**
 * REST Object resource class.
 * 
 * @author Mathias Edblom
 * @since 2.13.1
 */
@Path("account")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class TempoResource {

    /**
     * The logger
     */
    private static final Logger logger = LoggerFactory.getLogger(TempoResource.class);
    private final ImportSourceConfigurationFacade importSourceConfigurationFacade;
    private final IQLFacade iqlFacade;

    /**
     * Constructor.
     * 
     * @param pObjectRestService
     *            injected facade
     * @param pAssembler
     *            injected assembler
     */
    public TempoResource(final ImportSourceConfigurationFacade importSourceConfigurationFacade,
            final IQLFacade iqlFacade) {
        this.importSourceConfigurationFacade = importSourceConfigurationFacade;
        this.iqlFacade = iqlFacade;
    }

    /**
     * REST service finding all objects from object class.
     * 
     * @param uriInfo
     *            provides access to application and request URI information
     * @return a REST response object
     */
    @GET
    @Path("/{accountId}/objects")
    public Response findObjects(@PathParam("accountId") final Integer accountId, @Context final UriInfo uriInfo) {
        try {
            List<ObjectBean> objects = loadTempoAccountObjects(accountId);
            List<ObjectEntry> objectEntries = new ArrayList<ObjectEntry>();
            for (ObjectBean objectBean : objects) {
                ObjectEntry objectEntry = new ObjectEntry();
                objectEntry.setId(objectBean.getId());
                objectEntry.setObjectKey(objectBean.getObjectKey());
                objectEntry.setLabel(objectBean.getLabel());
                objectEntries.add(objectEntry);
            }

            return Response.ok(objectEntries).cacheControl(never()).build();

        } catch (Exception e) {
            return Response.serverError().cacheControl(never()).build();
        }
    }

    private List<ObjectBean> loadTempoAccountObjects(int accountId) throws InsightException {
        List<ImportSource> importSources = importSourceConfigurationFacade
                .findImportSourcesByModule(TempoAccountImportModule.TEMPO_IMPORT_MODULE_KEY);

        List<ObjectBean> objectBeans = new ArrayList<>();

        for (ImportSource importSource : importSources) {
            for (ImportSourceOT importSourceOT : importSource.getEnabledImportSourceOTS()) {
                if (StringUtils.equalsIgnoreCase(importSourceOT.getModuleOTSelector().getSelector(),
                        TempoSelector.ACCOUNT.getSelector())) {
                    Set<ObjectTypeAttributeBean> otas = new HashSet<>();
                    for (ImportSourceOTAttr attr : importSourceOT.getExternalIdConfigurations()) {
                        if (attr.getDataLocator() != null) {
                            Optional<DataLocator> dataLocator = attr.getDataLocator()
                                    .stream().filter(locator -> StringUtils
                                            .equalsIgnoreCase(AccountService.id.getLocator(), locator.getLocator()))
                                    .findFirst();
                            if (dataLocator.isPresent()) {
                                otas.add(attr.getObjectTypeAttributeBean());
                                break;
                            }
                        }
                    }

                    if (otas.isEmpty()) {
                        continue;
                    }

                    /**
                     * Start preparing the IQL to get Tempo Account objects
                     */
                    StringBuffer IQL = new StringBuffer("objectTypeId = " + importSourceOT.getObjectTypeBean().getId());

                    for (ObjectTypeAttributeBean ota : otas) {
                        IQL.append(" AND ");
                        IQL.append("\"");
                        IQL.append(ota.getName());
                        IQL.append("\"");
                        IQL.append(" = ");
                        IQL.append(String.valueOf(accountId));
                    }

                    objectBeans.addAll(iqlFacade.findObjectsByIQLAndSchema(
                            importSourceOT.getObjectTypeBean().getObjectSchemaId().intValue(), IQL.toString()));

                    break;
                }
            }
        }

        return objectBeans;

    }
}
