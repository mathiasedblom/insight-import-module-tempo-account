package com.riadalabs.jira.plugins.insight_tempo_accounts.panel;

import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.ExecutingHttpRequest;

public class TempoAccountInsightPanel extends AbstractUserProfilePanel {
    private static final Logger logger = LoggerFactory.getLogger(TempoAccountInsightPanel.class);

    @Inject
    public TempoAccountInsightPanel(JiraAuthenticationContext jiraAuthenticationContext) {
        super(jiraAuthenticationContext);
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {

        User currentUser = (User) context.get("user");
        Map<String, Object> params = createVelocityParams(currentUser, currentUser);

        try {
            Object account = context.get("account");
            Integer id = (Integer) account.getClass().getMethod("getId").invoke(account);
            params.put("accountId", id);
        } catch (Exception nsme) {
            logger.error("Can not get id:", nsme);
        }

        String contextPath = ExecutingHttpRequest.get().getContextPath();
        params.put("contextPath", contextPath);

        return params;
    }

}