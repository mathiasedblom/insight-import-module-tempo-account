package com.riadalabs.jira.plugins.insight_tempo_accounts.locator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tempoplugin.accounts.account.api.Account;

public class AccountLeadLocator extends JavaCodeTempoDataLocator<Account> {

    public AccountLeadLocator(String locator) {
        super(locator);
    }

    @Override
    public List<String> getCodeLocatorValue(Account account) {
        Set<String> values = new HashSet<>();

        if (account.getTempoLead() != null) {
            values.add(String.valueOf(account.getTempoLead().getUsername()));
        }

        return new ArrayList<>(values);
    }
}
