package com.riadalabs.jira.plugins.insight_tempo_accounts.locator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tempoplugin.accounts.account.api.Account;

public class AccountCustomerLocator extends JavaCodeTempoDataLocator<Account> {

    public AccountCustomerLocator(String locator) {
        super(locator);
    }

    @Override
    public List<String> getCodeLocatorValue(Account account) {
        Set<String> values = new HashSet<>();

        if (account.getCustomer() != null) {
            values.add(String.valueOf(account.getCustomer().getId()));
        }

        return new ArrayList<>(values);
    }
}
