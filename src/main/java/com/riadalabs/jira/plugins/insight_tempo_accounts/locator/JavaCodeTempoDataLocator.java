package com.riadalabs.jira.plugins.insight_tempo_accounts.locator;

import java.util.List;

public abstract class JavaCodeTempoDataLocator<T> extends TempoDataLocator {

    public JavaCodeTempoDataLocator(String locator) {
        super(locator);
        super.locatorType = LocatorType.JAVA_CODE_VALUE;
    }

    public abstract List<String> getCodeLocatorValue(T dataObject);

}
