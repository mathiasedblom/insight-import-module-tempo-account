package com.riadalabs.jira.plugins.insight_tempo_accounts.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

public class I18nHelper {

    private static I18nHelper instance;
    private static Properties properties;

    private I18nHelper() {
        InputStream inputStream = null;

        properties = new Properties();
        try {
            inputStream = this.getClass().getClassLoader().getResourceAsStream("properties/i18n.properties");
            properties.load(inputStream);
        } catch (IOException e) {
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    public static I18nHelper getInstance() {
        if (instance == null) {
            instance = new I18nHelper();
        }
        return instance;
    }

    public String getText(String name) {
        String value = properties.getProperty(name);
        return value != null ? value : name;
    }

    public String getText(String name, String param) {
        String value = getText(name, new Object[] { param });
        return value != null ? value : name;
    }

    public String getText(String name, Object[] substitutionParameters) {
        String value = properties.getProperty(name);

        if (value == null) {
            return name;
        }

        final MessageFormat mf;
        try {
            mf = new MessageFormat(value);
        } catch (final IllegalArgumentException e) {
            return name;
        }

        return mf.format((substitutionParameters == null) ? new Object[0] : substitutionParameters);

    }

}
