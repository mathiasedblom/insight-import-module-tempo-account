package com.riadalabs.jira.plugins.insight_tempo_accounts.util;

import java.util.Locale;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

public class I18nFactoryServiceImpl implements I18nFactoryService {

    private ApplicationProperties applicationProperties;
    private I18nHelper.BeanFactory beanFactory;

    public I18nFactoryServiceImpl(final ApplicationProperties applicationProperties,
            final I18nHelper.BeanFactory beanFactory) {
        this.applicationProperties = applicationProperties;
        this.beanFactory = beanFactory;
    }

    @Override
    public I18n getI18n(ApplicationUser user) {
        return new I18nHelperWrapper(this.beanFactory.getInstance(user));
    }

    @Override
    public I18n getI18n() {
        Locale defaultLocale = this.applicationProperties.getDefaultLocale();
        return getI18n(defaultLocale);
    }

    @Override
    public I18n getI18n(Locale locale) {
        return new I18nHelperWrapper(this.beanFactory.getInstance(locale));
    }

    static class I18nHelperWrapper implements I18n {
        private final I18nHelper helper;

        I18nHelperWrapper(I18nHelper helper) {
            this.helper = helper;
        }

        @Override
        public Locale getLocale() {
            return this.helper.getLocale();
        }

        @Override
        public String getText(String key) {
            return helper.getText(key);
        }

        @Override
        public String getText(String key, Object parameter) {
            return helper.getText(key, parameter);
        }

        @Override
        public String getText(String key, Object[] params) {
            return helper.getText(key, params);
        }

        @Override
        public String getText(String key, Object parameter1, Object parameter2) {
            return getText(key, new Object[] { parameter1, parameter2 });
        }

        @Override
        public String getText(String key, Object parameter1, Object parameter2, Object parameter3) {
            return getText(key, new Object[] { parameter1, parameter2, parameter3 });
        }

        @Override
        public String getText(String key, Object parameter1, Object parameter2, Object parameter3, Object parameter4) {
            return getText(key, new Object[] { parameter1, parameter2, parameter3, parameter4 });
        }

        @Override
        public String getText(String key, Object parameter1, Object parameter2, Object parameter3, Object parameter4,
                Object parameter5) {
            return getText(key, new Object[] { parameter1, parameter2, parameter3, parameter5 });
        }

        @Override
        public String getHelpText(String key) {
            return wrapWithHelpTags(this.getText(key));
        }

        @Override
        public String getHelpText(String key, Object parameter) {
            return wrapWithHelpTags(this.getText(key, parameter));
        }

        private boolean nullCheck(String key) {
            if (key == null) {
                I18nFactoryService.log.warn("null key passed to i18n.getText",
                        new IllegalArgumentException("The key argument must not be null"));
                return true;
            }

            return false;
        }

        /**
         * Surrounds a text section with {help}..{/help}.
         * 
         * This allows the client code that displays messages to the user to identify/parse this and display this
         * information in a custom way, for instance like a hoverable help icon.
         * 
         * @param text
         *            - The text to wrap.
         * @return A string surrounded with {help}..{/help}
         */
        private String wrapWithHelpTags(String i18nText) {
            return "{help}" + i18nText + "{/help}";
        }

    }
}