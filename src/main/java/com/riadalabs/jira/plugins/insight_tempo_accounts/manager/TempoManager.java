package com.riadalabs.jira.plugins.insight_tempo_accounts.manager;

import java.util.List;
import java.util.Map;

import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;

public interface TempoManager {

    List<DataLocator> getDataLocators();

    List<Map<DataLocator, List<String>>> getDataHolder();
}
