package com.riadalabs.jira.plugins.insight_tempo_accounts.locator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tempoplugin.accounts.account.api.Account;
import com.tempoplugin.accounts.link.api.AccountLink;

public class AccountProjectIdsLocator extends JavaCodeTempoDataLocator<Account> {

    public AccountProjectIdsLocator(String locator) {
        super(locator);
    }

    @Override
    public List<String> getCodeLocatorValue(Account account) {
        Set<String> values = new HashSet<>();

        if (account.getLinks() != null) {
            for (AccountLink link : account.getLinks()) {
                if (link.getScopeType() != null && "PROJECT".equals(link.getScopeType().name())) {
                    values.add(String.valueOf(link.getScope()));
                }
            }
        }

        return new ArrayList<>(values);
    }
}
