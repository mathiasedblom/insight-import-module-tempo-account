package com.riadalabs.jira.plugins.insight_tempo_accounts.manager;

import java.util.Collection;

import com.tempoplugin.accounts.attributes.api.Category;
import com.tempoplugin.accounts.attributes.api.CategoryService;
import com.tempoplugin.accounts.attributes.api.Customer;
import com.tempoplugin.accounts.attributes.api.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ImportComponentException;
import com.tempoplugin.accounts.account.api.Account;
import com.tempoplugin.accounts.account.api.AccountService;

public abstract class AbstractService {

    /**
     * The logger
     */
    private static final Logger logger = LoggerFactory.getLogger(AbstractService.class);

    public AbstractService() {
    }

    protected Collection<Account> getAccountData() {

        AccountService accountService = ComponentAccessor.getOSGiComponentInstanceOfType(AccountService.class);

        if (accountService == null) {
            throw new ImportComponentException("Tempo AccountService is not available!");
        }

        return accountService.getAllAccounts().get();
    }

    protected Collection<Customer> getCustomerData() {

        CustomerService customerService = ComponentAccessor.getOSGiComponentInstanceOfType(CustomerService.class);

        if (customerService == null) {
            throw new ImportComponentException("Tempo CustomerService is not available!");
        }

        return customerService.getAttributes().get();
    }

    protected Collection<Category> getCategoryData() {

        CategoryService categoryService = ComponentAccessor.getOSGiComponentInstanceOfType(CategoryService.class);

        if (categoryService == null) {
            throw new ImportComponentException("Tempo CategoryService is not available!");
        }

        return categoryService.getAttributes().get();
    }

}
