package com.riadalabs.jira.plugins.insight_tempo_accounts.condition;

import org.apache.log4j.Logger;

import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

public abstract class AbstractInsightCondition extends AbstractWebCondition {

    public static final Logger log = Logger.getLogger(AbstractInsightCondition.class);

    public AbstractInsightCondition() {
    }

    @Override
    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
        return condition(user, jiraHelper);
    }

    abstract boolean condition(ApplicationUser paramUser, JiraHelper paramJiraHelper);

}