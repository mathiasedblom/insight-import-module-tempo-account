package com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl;

import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.CategoryCategoryTypeLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.JavaCodeTempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.StandardTempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.TempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.AbstractService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.TempoManager;
import com.tempoplugin.accounts.attributes.api.Category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryService extends AbstractService implements TempoManager {

    public static TempoDataLocator name = new StandardTempoDataLocator("Name", "name");
    public static TempoDataLocator id = new StandardTempoDataLocator("Tempo Id", "id");
    public static TempoDataLocator key = new StandardTempoDataLocator("Tempo Key", "key");
    public static JavaCodeTempoDataLocator<Category> categoryType = new CategoryCategoryTypeLocator("CategoryType Id");

    private TempoDataLocator[] locators = new TempoDataLocator[] {name, id, key, categoryType};

    public CategoryService() {
    }

    @Override
    public List<DataLocator> getDataLocators() {
        return Arrays.asList(locators);
    }

    @SuppressWarnings ("unchecked")
    @Override
    public List<Map<DataLocator, List<String>>> getDataHolder() {

        List<Map<DataLocator, List<String>>> dataMap = new ArrayList<>();

        for (Category category : getCategoryData()) {
            Map<DataLocator, List<String>> keyValueMap = new HashMap<>();
            for (TempoDataLocator tempoDataLocator : locators) {
                switch (tempoDataLocator.getLocatorType()) {
                    case JAVA_METHOD_VALUE:
                        keyValueMap.put(tempoDataLocator,
                                ((StandardTempoDataLocator) tempoDataLocator).getMethodValue(category));
                        break;
                    case JAVA_CODE_VALUE:
                        keyValueMap.put(tempoDataLocator,
                                ((JavaCodeTempoDataLocator<Category>) tempoDataLocator).getCodeLocatorValue(category));

                        break;
                    default:
                        break;

                }

            }

            dataMap.add(new HashMap<>(keyValueMap));

        }

        return dataMap;
    }

}
