package com.riadalabs.jira.plugins.insight_tempo_accounts;

import java.util.Collections;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.atlassian.adapter.jackson.ObjectMapper;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ImportModuleConfiguration;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.insightfield.InsightFieldConfiguration;

public class TempoConfiguration implements ImportModuleConfiguration {
    private static final long serialVersionUID = -4473533132810886240L;

    // private InsightFieldConfiguration exampleField;

    /**
     * We dont need any module fields in this case but this is an example of a text field
     */
    public TempoConfiguration() {
        // exampleField = new InsightFieldTextConfiguration("exampleField",
        // "Example Field",
        // "This is the example field");
        // exampleField.setMandatory(true);

    }

    @Override
    public String toJSON() {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    @Override
    @JsonIgnore
    public List<InsightFieldConfiguration> getFieldsConfiguration() {
        return Collections.emptyList();
    }

}
