package com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl;

import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.JavaCodeTempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.StandardTempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.TempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.AbstractService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.TempoManager;
import com.tempoplugin.accounts.attributes.api.Category;
import com.tempoplugin.accounts.attributes.api.CategoryType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryTypeService extends AbstractService implements TempoManager {

    public static TempoDataLocator name = new StandardTempoDataLocator("Name", "name");
    public static TempoDataLocator id = new StandardTempoDataLocator("Tempo Id", "id");
    public static TempoDataLocator color = new StandardTempoDataLocator("Color", "color");

    private TempoDataLocator[] locators = new TempoDataLocator[] {name, id, color};

    public CategoryTypeService() {
    }

    @Override
    public List<DataLocator> getDataLocators() {
        return Arrays.asList(locators);
    }

    @SuppressWarnings ("unchecked")
    @Override
    public List<Map<DataLocator, List<String>>> getDataHolder() {

        List<Map<DataLocator, List<String>>> dataMap = new ArrayList<>();

        for (Category category : getCategoryData()) {
            if (category.getCategoryType() != null) {

                Map<DataLocator, List<String>> keyValueMap = new HashMap<>();
                for (TempoDataLocator tempoDataLocator : locators) {
                    switch (tempoDataLocator.getLocatorType()) {
                        case JAVA_METHOD_VALUE:
                            keyValueMap.put(tempoDataLocator,
                                    ((StandardTempoDataLocator) tempoDataLocator).getMethodValue(
                                            category.getCategoryType()));
                            break;
                        case JAVA_CODE_VALUE:
                            keyValueMap.put(tempoDataLocator,
                                    ((JavaCodeTempoDataLocator<CategoryType>) tempoDataLocator).getCodeLocatorValue(
                                            category.getCategoryType()));

                            break;
                        default:
                            break;

                    }

                }

                dataMap.add(new HashMap<>(keyValueMap));
            }
        }

        return dataMap;
    }

}
