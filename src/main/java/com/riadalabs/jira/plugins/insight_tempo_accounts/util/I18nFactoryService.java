package com.riadalabs.jira.plugins.insight_tempo_accounts.util;

import java.util.Locale;

import org.apache.log4j.Logger;

import com.atlassian.jira.user.ApplicationUser;

public abstract interface I18nFactoryService {
    public static final Logger log = Logger.getLogger(I18nFactoryService.class);

    public abstract I18n getI18n(ApplicationUser paramUser);

    public abstract I18n getI18n(Locale paramLocale);

    public abstract I18n getI18n();
}