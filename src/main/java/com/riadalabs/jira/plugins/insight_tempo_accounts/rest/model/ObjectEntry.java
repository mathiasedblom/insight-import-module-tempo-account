package com.riadalabs.jira.plugins.insight_tempo_accounts.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mathias Edblom
 */
@XmlRootElement(name = "Object")
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjectEntry {

    /** ID. */
    @XmlElement
    private Integer id;

    /** Name. */
    @XmlElement
    private String label;

    /** Object Key. */
    @XmlElement
    private String objectKey;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

}
