package com.riadalabs.jira.plugins.insight_tempo_accounts.locator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tempoplugin.accounts.attributes.api.Category;

public class CategoryCategoryTypeLocator extends JavaCodeTempoDataLocator<Category> {

    public CategoryCategoryTypeLocator(String locator) {
        super(locator);
    }

    @Override
    public List<String> getCodeLocatorValue(Category category) {
        Set<String> values = new HashSet<>();
        if (category != null && category.getCategoryType() != null) {
            values.add(String.valueOf(category.getCategoryType().getId()));
        }
        return new ArrayList<>(values);
    }
}
