package com.riadalabs.jira.plugins.insight_tempo_accounts.condition;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.InsightPermissionFacade;

public class CanViewInsight extends AbstractInsightCondition {

    /** insight Permission Service */
    private final InsightPermissionFacade insightPermissionFacade = ComponentAccessor
            .getOSGiComponentInstanceOfType(InsightPermissionFacade.class);

    @Override
    boolean condition(ApplicationUser user, JiraHelper jiraHelper) {
        return this.insightPermissionFacade.hasInsightViewPermisson(user);
    }

}