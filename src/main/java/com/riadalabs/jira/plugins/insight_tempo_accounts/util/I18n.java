package com.riadalabs.jira.plugins.insight_tempo_accounts.util;

import java.util.Locale;

public abstract interface I18n {
    public abstract Locale getLocale();

    public abstract String getText(String paramString);

    public abstract String getText(String paramString, Object paramObject);

    public abstract String getText(String paramString, Object paramObject1, Object paramObject2);

    public abstract String getText(String paramString, Object paramObject1, Object paramObject2, Object paramObject3);

    public abstract String getText(String paramString, Object paramObject1, Object paramObject2, Object paramObject3,
            Object paramObject4);

    public abstract String getText(String paramString, Object paramObject1, Object paramObject2, Object paramObject3,
            Object paramObject4, Object paramObject5);

    public abstract String getText(String paramString, Object[] paramArrayOfObject);

    public abstract String getHelpText(String paramString);

    public abstract String getHelpText(String paramString, Object paramObject);

}