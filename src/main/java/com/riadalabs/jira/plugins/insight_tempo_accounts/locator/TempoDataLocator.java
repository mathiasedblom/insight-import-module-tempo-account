package com.riadalabs.jira.plugins.insight_tempo_accounts.locator;

import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;

public class TempoDataLocator extends DataLocator {

    public enum LocatorType {
        JAVA_METHOD_VALUE,
        JAVA_CODE_VALUE
    }

    protected LocatorType locatorType;

    public TempoDataLocator(String locator) {
        super(locator);
    }

    public static boolean isEmpty(String text) {
        return text == null || "".equals("");
    }

    public LocatorType getLocatorType() {
        return locatorType;
    }

}
