package com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl;

import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.JavaCodeTempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.StandardTempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.TempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.AbstractService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.TempoManager;
import com.tempoplugin.accounts.attributes.api.Customer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerService extends AbstractService implements TempoManager {

    public static TempoDataLocator name = new StandardTempoDataLocator("Name", "name");
    public static TempoDataLocator id = new StandardTempoDataLocator("Tempo Id", "id");
    public static TempoDataLocator key = new StandardTempoDataLocator("Tempo Key", "key");

    private TempoDataLocator[] locators = new TempoDataLocator[] {name, id, key};

    public CustomerService() {
    }

    @Override
    public List<DataLocator> getDataLocators() {
        return Arrays.asList(locators);
    }

    @SuppressWarnings ("unchecked")
    @Override
    public List<Map<DataLocator, List<String>>> getDataHolder() {

        List<Map<DataLocator, List<String>>> dataMap = new ArrayList<>();

        for (Customer customer : getCustomerData()) {
            Map<DataLocator, List<String>> keyValueMap = new HashMap<>();
            for (TempoDataLocator tempoDataLocator : locators) {
                switch (tempoDataLocator.getLocatorType()) {
                    case JAVA_METHOD_VALUE:
                        keyValueMap.put(tempoDataLocator,
                                ((StandardTempoDataLocator) tempoDataLocator).getMethodValue(customer));
                        break;
                    case JAVA_CODE_VALUE:
                        keyValueMap.put(tempoDataLocator,
                                ((JavaCodeTempoDataLocator<Customer>) tempoDataLocator).getCodeLocatorValue(customer));

                        break;
                    default:
                        break;

                }

            }

            dataMap.add(new HashMap<>(keyValueMap));

        }

        return dataMap;
    }

}
