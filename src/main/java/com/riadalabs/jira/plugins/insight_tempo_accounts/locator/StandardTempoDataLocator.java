package com.riadalabs.jira.plugins.insight_tempo_accounts.locator;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

public class StandardTempoDataLocator extends TempoDataLocator {

    /**
     * The logger
     */
    private static final Logger logger = LoggerFactory.getLogger(StandardTempoDataLocator.class);

    private final String methodName;

    public StandardTempoDataLocator(String locator, String methodName) {
        super(locator);
        super.locatorType = LocatorType.JAVA_METHOD_VALUE;
        this.methodName = Objects.requireNonNull(methodName);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<String> getMethodValue(Object tempoObject) {
        if (tempoObject == null) {
            logger.info("Got null object as object for method, will return empty list");
            return Lists.newArrayList();
        }
        try {
            Object value = PropertyUtils.getProperty(tempoObject, methodName);
            if (value != null) {
                if (value instanceof List) {
                    return (List<String>) ((List) value).stream().map(object -> Objects.toString(object, null))
                            .collect(Collectors.toList());
                } else {
                    return Arrays.asList(value.toString());
                }
            }
        } catch (Exception ee) {
            logger.warn("Could not get locator value", ee);
        }
        return Lists.newArrayList();

    }

}
