package com.riadalabs.jira.plugins.insight_tempo_accounts.workflow;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.riadalabs.jira.plugins.insight.services.model.ObjectAttributeBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectBean;
import com.tempoplugin.accounts.account.api.Account;
import com.tempoplugin.accounts.account.api.AccountService;

public class WorkflowFunction implements Condition, Validator, FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowFunction.class);
    private final CustomFieldManager customFieldManager;
    private final IssueManager issueManager;
    /** Security Context */
    private final JiraAuthenticationContext authCtx;
    private final ObjectFacade objectFacade;

    public WorkflowFunction(final CustomFieldManager customFieldManager, final IssueManager issueManager,
            final JiraAuthenticationContext authCtx, final ObjectFacade objectFacade) {

        this.customFieldManager = customFieldManager;
        this.issueManager = issueManager;
        this.authCtx = authCtx;
        this.objectFacade = objectFacade;
    }

    private MutableIssue getIssue(Map transientVars) {
        return (MutableIssue) transientVars.get("issue");
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {

        try {

            MutableIssue issue = getIssue(transientVars);
            String type = (String) args.get("field.type");
            if (type == null) {
                type = String.valueOf(PostFunctionFactory.TYPE_SET_TEMPO_ACCOUNT);
            }
            int iType = Integer.parseInt(type);

            switch (iType) {
            case PostFunctionFactory.TYPE_SET_TEMPO_ACCOUNT:

                String attributeName = (String) args.get("field.attributeName");
                String tempoCustomFieldId = (String) args.get("field.tempoCustomFieldId");
                String insightCustomFieldId = (String) args.get("field.insightCustomFieldId");

                CustomField tempoCustomField = customFieldManager.getCustomFieldObject(tempoCustomFieldId);
                if (tempoCustomField != null) {
                    CustomField customField = customFieldManager.getCustomFieldObject(insightCustomFieldId);
                    List<ObjectBean> objectBeans = (List<ObjectBean>) issue.getCustomFieldValue(customField);

                    AccountService accountService = ComponentAccessor
                            .getOSGiComponentInstanceOfType(AccountService.class);

                    Account account = null;
                    for (ObjectBean objectBean : objectBeans) {
                        ObjectAttributeBean objectAttributeBean = objectFacade
                                .loadObjectAttributeBean(objectBean.getId(), attributeName);
                        if (objectAttributeBean != null) {
                            try {
                                Integer tempoId = Integer.valueOf(String
                                        .valueOf(objectAttributeBean.getObjectAttributeValueBeans().get(0).getValue()));

                                account = accountService.getAccountById(tempoId).get();
                                if (account != null) {
                                    break;
                                }
                            } catch (Exception ee) {
                                logger.warn("Could not store Tempo Account due to: ", ee);
                            }
                        }
                    }

                    issue.setCustomFieldValue(tempoCustomField, account);
                    issueManager.updateIssue(authCtx.getLoggedInUser(), issue, EventDispatchOption.DO_NOT_DISPATCH,
                            false);
                }
                break;
            default:
                break;
            }

        } catch (Exception ee) {
            if (logger.isDebugEnabled()) {
                logger.debug("Post function could not execute: ", ee);
            } else if (logger.isWarnEnabled()) {
                logger.warn("Post function could not execute: " + ee.getMessage());
            }
        }

    }

    @Override
    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException, WorkflowException {
    }

    @Override
    public boolean passesCondition(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        return true;
    }

}