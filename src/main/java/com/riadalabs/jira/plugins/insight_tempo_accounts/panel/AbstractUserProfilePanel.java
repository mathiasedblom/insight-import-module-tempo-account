package com.riadalabs.jira.plugins.insight_tempo_accounts.panel;

import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;

public abstract class AbstractUserProfilePanel implements ContextProvider {
    protected final JiraAuthenticationContext jiraAuthenticationContext;

    public AbstractUserProfilePanel(JiraAuthenticationContext jiraAuthenticationContext) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public void init(Map<String, String> params) throws PluginParseException {
    }

    protected Map<String, Object> createVelocityParams(User profileUser, User currentUser) {
        Map<String, Object> velocityParams = JiraVelocityUtils.getDefaultVelocityParams(this.jiraAuthenticationContext);
        velocityParams.put("profileUser", profileUser);
        velocityParams.put("currentUser", currentUser);
        velocityParams.put("i18n", this.jiraAuthenticationContext.getI18nHelper());
        return velocityParams;
    }
}