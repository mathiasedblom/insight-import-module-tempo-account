package com.riadalabs.jira.plugins.insight_tempo_accounts.workflow;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.riadalabs.jira.plugins.insight_tempo_accounts.util.I18nFactoryService;

public class PostFunctionFactory extends AbstractWorkfowFactory implements WorkflowPluginFunctionFactory {

    public static final int TYPE_SET_TEMPO_ACCOUNT = 0;

    protected JiraAuthenticationContext authCtx;
    protected final I18nFactoryService i18nFactoryService;

    public PostFunctionFactory(final CustomFieldManager customFieldManager, JiraAuthenticationContext authCtx,
            I18nFactoryService i18nFactoryService) {
        super(customFieldManager);
        this.i18nFactoryService = i18nFactoryService;
        this.authCtx = authCtx;
    }

    @Override
    public Map<String, String> getDescriptorParams(Map<String, Object> conditionParams) {

        Map<String, String> params = new HashMap<String, String>();
        String postFunctionType = extractSingleParam(conditionParams, "type");
        int type = (postFunctionType != null) ? Integer.parseInt(postFunctionType) : TYPE_SET_TEMPO_ACCOUNT;
        params.put("field.type", String.valueOf(type));

        if (type == PostFunctionFactory.TYPE_SET_TEMPO_ACCOUNT) {
            params.put("field.attributeName", extractSingleParam(conditionParams, "attributeName"));
            params.put("field.tempoCustomFieldId", extractSingleParam(conditionParams, "tempoCustomFieldId"));
            params.put("field.insightCustomFieldId", extractSingleParam(conditionParams, "insightCustomFieldId"));
        }

        return params;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        super.getVelocityParamsForInput(velocityParams);
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor inDescriptor) {

        FunctionDescriptor descriptor = (FunctionDescriptor) inDescriptor;

        String type = (String) descriptor.getArgs().get("field.type");
        if (type == null) {
            type = String.valueOf(TYPE_SET_TEMPO_ACCOUNT);
        }
        velocityParams.put("type", type);

        if (Integer.parseInt(type) == PostFunctionFactory.TYPE_SET_TEMPO_ACCOUNT) {

            String attributeName = (String) descriptor.getArgs().get("field.attributeName");
            velocityParams.put("attributeName", attributeName);

            String tempoCustomFieldId = (String) descriptor.getArgs().get("field.tempoCustomFieldId");
            if (tempoCustomFieldId != null) {
                CustomField customField = customFieldManager.getCustomFieldObject(tempoCustomFieldId);
                velocityParams.put("tempoCustomField", customField != null ? customField : null);
            }

            String insightCustomFieldId = (String) descriptor.getArgs().get("field.insightCustomFieldId");
            if (insightCustomFieldId != null) {
                CustomField customField = customFieldManager.getCustomFieldObject(insightCustomFieldId);
                velocityParams.put("insightCustomField", customField != null ? customField : null);
            }

        }
    }

}