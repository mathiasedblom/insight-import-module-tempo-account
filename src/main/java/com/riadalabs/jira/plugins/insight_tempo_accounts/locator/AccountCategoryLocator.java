package com.riadalabs.jira.plugins.insight_tempo_accounts.locator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.tempoplugin.accounts.account.api.Account;

public class AccountCategoryLocator extends JavaCodeTempoDataLocator<Account> {

    public AccountCategoryLocator(String locator) {
        super(locator);
    }

    @Override
    public List<String> getCodeLocatorValue(Account account) {
        Set<String> values = new HashSet<>();

        if (account.getCategory() != null) {
            values.add(String.valueOf(account.getCategory().getId()));
        }

        return new ArrayList<>(values);
    }
}
