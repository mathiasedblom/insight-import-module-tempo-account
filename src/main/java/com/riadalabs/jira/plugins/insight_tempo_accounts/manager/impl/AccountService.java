package com.riadalabs.jira.plugins.insight_tempo_accounts.manager.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.AccountCategoryLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.AccountContactLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.AccountCustomerLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.AccountLeadLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.AccountProjectIdsLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.JavaCodeTempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.StandardTempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.locator.TempoDataLocator;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.AbstractService;
import com.riadalabs.jira.plugins.insight_tempo_accounts.manager.TempoManager;
import com.tempoplugin.accounts.account.api.Account;

public class AccountService extends AbstractService implements TempoManager {

    public static TempoDataLocator name = new StandardTempoDataLocator("Name", "name");
    public static TempoDataLocator id = new StandardTempoDataLocator("Tempo Id", "id");
    public static TempoDataLocator key = new StandardTempoDataLocator("Tempo Key", "key");
    public static TempoDataLocator status = new StandardTempoDataLocator("Status", "status");
    public static TempoDataLocator global = new StandardTempoDataLocator("Global", "global");
    public static TempoDataLocator monthlyBudget = new StandardTempoDataLocator("Monthly Budget", "monthlyBudget");
    public static TempoDataLocator projectIds = new AccountProjectIdsLocator("Linked Project Ids");
    public static JavaCodeTempoDataLocator<Account> lead = new AccountLeadLocator("Lead Username");
    public static JavaCodeTempoDataLocator<Account> contact = new AccountContactLocator("Contact Username");
    public static JavaCodeTempoDataLocator<Account> customer = new AccountCustomerLocator("Customer Id");
    public static JavaCodeTempoDataLocator<Account> category = new AccountCategoryLocator("Category Id");

    private TempoDataLocator[] locators = new TempoDataLocator[] { name, id, key, status, global, monthlyBudget,
            projectIds, lead, contact, customer, category };

    public AccountService() {
    }

    @Override
    public List<DataLocator> getDataLocators() {
        return Arrays.asList(locators);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Map<DataLocator, List<String>>> getDataHolder() {

        List<Map<DataLocator, List<String>>> dataMap = new ArrayList<>();

        for (Account account : getAccountData()) {
            Map<DataLocator, List<String>> keyValueMap = new HashMap<>();
            for (TempoDataLocator tempoDataLocator : locators) {
                switch (tempoDataLocator.getLocatorType()) {
                case JAVA_METHOD_VALUE:
                    keyValueMap.put(tempoDataLocator,
                            ((StandardTempoDataLocator) tempoDataLocator).getMethodValue(account));
                    break;
                case JAVA_CODE_VALUE:
                    keyValueMap.put(tempoDataLocator,
                            ((JavaCodeTempoDataLocator<Account>) tempoDataLocator).getCodeLocatorValue(account));

                    break;
                default:
                    break;

                }

            }

            dataMap.add(new HashMap<DataLocator, List<String>>(keyValueMap));

        }

        return dataMap;
    }

}
